<?php

/**
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */

$MOD_LIB_CODEMIRROR = array(
    "theme" => "Theme",
    "mode"  => "Mode",
    "save"  => "Save CodeMirror setting",
    "result_prefix" => "Callback result: ",
    "ajax_saved_ok"  => "Ok - settings saved for this section [%s] and this module ('%s').<br >You may have to reload the page to see effect.",
    "overview_description"  => "Overview of the installed/available themes of CodeMirror within this installation."
);
