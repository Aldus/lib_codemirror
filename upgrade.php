<?php

/**
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$database = LEPTON_database::getInstance();
$info = [];
$database->describe_table(
    TABLE_PREFIX . "mod_lib_codemirror",
    $info,
    LEPTON_database::DESCRIBE_ONLY_NAMES
);

if (!in_array("active", $info))
{
    // Field doesn't exists - so we have to alter the table.
    $database->simple_query("ALTER TABLE `" . TABLE_PREFIX . "mod_lib_codemirror` ADD `active` INT(1)  NULL  DEFAULT 1  AFTER `mode`");
}
