<?php

declare(strict_types=1);

/**
 *  Simple class of module lib_codemirror to generate a overview(-html)
 *  of all installed themes.
 *
 *
 * @package         LEPTON-CMS - private modules
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */
class lib_codemirror_overview
{
    /**
     *  Static function to get a generated list of all installed CodeMirror-Themes.  
     *  (Or only the favorites).
     *  Use the "overview.lte" inside the "template" folder to customize the output;
     *  or use the L* way to place a own one inside "~frontend/lib_codemirror/overview.lte"
     *  of your frontend template.
     *
     *  @param  boolean $bUseOnlyFavorites  Flag to return only the favorites list.
     *                                      Default is 'false', so all themes are generated.
     *  @return string  Generated HTML code.
     *
	 *	@code{.php}
     *
     *  $sHTML = lib_codemirror_overview::getOverview();
     *
     *  echo $sHTML;
     *
	 *	@endcode
     *
     */
    static function getOverview(bool $bUseOnlyFavorites = false): string
    {
        $oCODEMIRROR = lib_codemirror::getInstance();
    
        $oCODEMIRROR->useFavorites = $bUseOnlyFavorites;
    
        $aBasicFiles = $oCODEMIRROR->getBaseFiles();
    
        $aAllThemes = [];
        foreach($aBasicFiles['css'] as $f)
        {
            $aTemp1 = explode("/", $f);
            $aTemp2 = array_pop($aTemp1);
            $aTemp3 = explode(".", $aTemp2);
            $aAllThemes[] = array_shift($aTemp3);
        }
        
        // Skip first two elements as this one belongs to the codemirror-basic.css!
        array_shift($aAllThemes);
        array_shift($aAllThemes);
    
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule("lib_codemirror");
    
        return $oTWIG->render(
            "@lib_codemirror/overview.lte",
            [
                'VERSION'       => VERSION,
                'oCODEMIRROR'   => $oCODEMIRROR,
                'aBasicFiles'   => $aBasicFiles,
                'allThemes'     => $aAllThemes
            ]
        );
    }
}