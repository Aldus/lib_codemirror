<?php

declare(strict_types=1);

/**
 * Interface class of module lib_codemirror for LEPTON-CMS.
 *
 *
 * @package         LEPTON-CMS - private modules
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */

class lib_codemirror_interface extends lib_codemirror
{
    const LCM_DISPLAY_NONE          = 0;
    const LCM_DISPLAY_THEME_SELECT  = 1;
    const LCM_DISPLAY_MODE_SELECT   = 2;
    const LCM_DISPLAY_SAVE          = 4;
    const LCM_DISPLAY_ALL           = 7;
    
    static int      $error_num  = 0;
    static string   $error_msg  = "";
      
    /**
     *  Own instance of this module.
     *  @var    object  $instance
     *
     */
    static $instance;
    
    /**
     *  Inherited function from LEPTON_abstract.
     *
     */
    public function initialize()
    {
        parent::$instance->getThemes();
        self::$instance->themes =& parent::$instance->themes;
        self::$instance->defaultTheme =& parent::$instance->defaultTheme;
        self::$instance->modes =& parent::$instance->modes;
        self::$instance->useFavorites =& parent::$instance->useFavorites;
    }

    /**
     *  Returns the complete HTML source for the theme- and mode selects, the save-button  
     *  and the js-code to run in a given backend-interface, e.g. Code2.
     *
     *  @param  string  $sModuleName    A valid modulename.
     *  @param  int     $iSectionID     A valid section-id.
     *  @param  string  $sJsCallback    A call-back-functionname.
     *  @param  int     $iDisplayMode   A displaymode: see class-constants.
     *
     *  @return string  The generated HTML-code.
     *
     */
    public function buildInterface(
        string $sModuleName = "code2",
        int $iSectionID = 0,
        string $sJsCallback = "",
        int $iDisplayMode = self::LCM_DISPLAY_ALL
    ): string
    {
        $aTempSectionInfo = self::getSectionSettings( $iSectionID );
       
        $data = [
            "oCODEMIRROR"   => $this,
            "section_id"    => $iSectionID,
            "module_name"   => $sModuleName,
            "mode_id"       => "aldus2".$iSectionID,
            "theme_id"      => "aldus".$iSectionID,
            "themes"        => ( true === $this->useFavorites ) ? $this->favoritesThemes : $this->themes,
            "modes"         => ( true === $this->useFavorites ) ? $this->favoritesModes : $this->modes,
            "sJCallback"    => $sJsCallback,
            "display"       => [
                'mode'  => (($iDisplayMode & self::LCM_DISPLAY_MODE_SELECT) == self::LCM_DISPLAY_MODE_SELECT),
                'theme' => (($iDisplayMode & self::LCM_DISPLAY_THEME_SELECT) == self::LCM_DISPLAY_THEME_SELECT),
                'save'  => (($iDisplayMode & self::LCM_DISPLAY_SAVE) == self::LCM_DISPLAY_SAVE)
            ],
            "selected"   => [
                'mode'  => ($aTempSectionInfo['mode'] ?? $this->defaultMode),
                'theme' => ($aTempSectionInfo['theme'] ?? $this->defaultTheme)
            ],
            "active"    => $aTempSectionInfo['active'] ?? 1,
            "hash" =>   $this->buildHash( $iSectionID )
        ];
        
        $oTWIG = lib_twig_box::getInstance();
        $oTWIG->registerModule("lib_codemirror");

        $sTemplateName = ((DEFAULT_THEME === "algos") || (DEFAULT_THEME === "talgos"))
            ? "algos_cm_select.lte"
            : "cm_select.lte"
            ;
            
        return $oTWIG->render(
            "@lib_codemirror/".$sTemplateName,
            $data
        );
    }

    /**
     *  Returns the db entry for a given section id.
     *
     *  @param  int     $iSectionID  A valid sectionID. (default is 0)
     *  @return array   An assoc. array with the values - or an empty array if none found.
     *  @access static
     *
     */
    static public function getSectionSettings(int $iSectionID = 0): array
    {
        $aSettings = [];
        
        LEPTON_database::getInstance()->execute_query(
            "SELECT `id`, `module`, `theme`, `mode`, `active` FROM `" . TABLE_PREFIX . "mod_lib_codemirror` WHERE `section_id` = " . $iSectionID,
            true,
            $aSettings,
            false
        );
        
        return $aSettings;
    }

    /**
     * @param int $iSectionID
     * @return array
     */
    private function buildHash(int $iSectionID): array
    {
        $sTempTime = TIME();
        $sTempTimeHash = sha1((string)$sTempTime);
        $sSecHash = password_hash($sTempTimeHash, PASSWORD_DEFAULT);
        
        // Houston: L* IV rocks!
        LEPTON_handle::register("random_string");
        $field_name = random_string(16);
        
        $_SESSION['codemirror_interface_x'][$iSectionID] = [
            'cm_t' => $sTempTime,
            'cm_v' => $sSecHash,
            'cm_n' => $field_name
        ];

        return ['time' => $sTempTime, 'hash' => $sTempTimeHash, 'fieldname' => $field_name];
    }
    
    /**
     *  Static function to test the "incoming" ajax-calls via $_post for a given section_id.
     *
     *  @param  int     $iSectionID     A valid section_id.
     *  @return boolean                 True if matched, otherwise false if something doesn't fit.
     *
     */
    static public function testHash(int $iSectionID = 0): bool
    {
        if ($iSectionID < 1)
        {
            self::$error_num = -10;
            self::$error_msg = "no valid section id";
            
            return false;
        }

        if (!isset($_SESSION['codemirror_interface_x'][$iSectionID]))
        {
            self::$error_num = -101;
            self::$error_msg = "no session entry found";

            return false;
        
        } else {
            if (!isset($_POST['ttime']))
            {
                self::$error_num = -11;
                self::$error_msg = "no time given";
                
                return false;
            } else {
                $iTestTime = intval($_POST['ttime']);
                $iNow = time();
                if (($iTestTime + (60 * 60)) < $iNow)
                {
                    self::$error_num = -12;
                    self::$error_msg = "timeout";
                    return false;
                } else {

                    $sLookupname = $_SESSION['codemirror_interface_x'][$iSectionID]['cm_n'];

                    if (!isset($_POST[$sLookupname]))
                    {
                        self::$error_num = -13;
                        self::$error_msg = "no session var for this section found";
                        return false;
                    }

                    $sTestval = $_POST[$sLookupname];
                    
                    $bIsOk = password_verify(
                        $sTestval,
                        $_SESSION['codemirror_interface_x'][$iSectionID]['cm_v']
                    );
                    if (false === $bIsOk)
                    {
                        self::$error_num = -14;
                        self::$error_msg = "hash failed";
                    }
                    
                    return $bIsOk;
                }
            }
        }
        return false;
    }
}
