<?php

declare(strict_types=1);

/**
 *  Custom class of module lib_codemirror for LEPTON-CMS.
 *
 *  @module lib_codemirror
 *
 */

class lib_codemirror_custom_settings
{
    // [1] A reduce list of themes
    const CUSTOM_FAVORITES = [
        'abcdef.css',
        'ayu-dark.css',
        'paraiso-light.css',
        'seti.css',
        'vibrant-ink.css',
        'xq-dark.css',
        'yeti.css'
    ];

    // [2] Own default theme
    const CUSTOM_DEFAULT = "ayu-dark";
    
}
