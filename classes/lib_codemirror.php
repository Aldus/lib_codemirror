<?php

declare(strict_types=1);

/**
 * Basic class of module lib_codemirror for LEPTON-CMS.
 *
 *
 * @package         LEPTON-CMS - private modules
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */

class lib_codemirror extends LEPTON_abstract
{
    /**
     *  Holds an linear array with all cm themes inside the CodeMirror root.
     *  @var    array   $themes 
     *
     */
    public array $themes = [];

    /**
     *  The default theme name.
     *  @var    string  $defaultTheme
     *
     */    
    public string $defaultTheme = "twilight";
    
    /**
     *  Holds the top 6 themes.
     *  @var    array   $favoritesThemes
     *
     */
    public array $favoritesThemes = [
        '3024-day.css',
        '3024-night.css',
        'abcdef.css',
        'ambiance.css',
        'ayu-dark.css',
        'ayu-mirage.css',
        'base16-dark.css',
        'base16-light.css',
        'bespin.css',
        'elegant.css',
        'mbo.css',
        'neo.css',
        'nord.css',
        'paraiso-light.css',
        'pastel-on-dark.css',
        'seti.css',
        'shadowfox.css',
        'solarized.css',
        'twilight.css',
        'vibrant-ink.css',
        'xq-dark.css',
        'yeti.css',
        'zenburn.css'
    ];
    
    /**
     *  Use the favorites list/array or all?
     *  Default is "false".
     *
     *  @var   boolean  $useFavorites
     *
     */
    public bool $useFavorites = true;
    
    /**
     *  Linear array that contains the supported modes.
     *  @var   array    $modes
     */
    public array $modes = [];
    
    /**
     *  Array that holds a reduces set of modes.
     *  @var   array    $favoritesModes
     */
    public array $favoritesModes = [
        'php',
        'xml',
        'htmlmixed',
        'javascript',
        'twig'
    ];
    
    /**
     *  The default initial mode to use.
     *  @var    string  $defaultMode
     */
    public string $defaultMode = "php";
    
    /**
     *  Name of the db-table without the TABLE_PREFIX.
     *  @var    string  $tableName
     */
    private string $tableName = "mod_lib_codemirror";
    
    /**
     *  Own instance of this module.
     *  @var    object  $instance
     *
     */
    static $instance;
    
    /**
     *  Inherited function from LEPTON_abstract.
     *
     */
    public function initialize()
    {
        self::$instance->getThemes();
        self::$instance->getModes();
    }

    /**
     *  Get the theme-files.
     *  Make use if LEPTON internal core function 'file_list'.
     *
     */
    public function getThemes(): void
    {
        $base_path = LEPTON_PATH . '/modules/lib_codemirror/CodeMirror/theme';

        if ((class_exists("lib_codemirror_custom_settings", true)) && (NULL !== lib_codemirror_custom_settings::CUSTOM_FAVORITES))
        {
            self::$instance->themes          = lib_codemirror_custom_settings::CUSTOM_FAVORITES;
            self::$instance->favoritesThemes = lib_codemirror_custom_settings::CUSTOM_FAVORITES;
            self::$instance->defaultTheme    = lib_codemirror_custom_settings::CUSTOM_DEFAULT;
        }
        else 
		{
            LEPTON_handle::register("file_list");
            self::$instance->themes = file_list($base_path, array(), false, "css", $base_path . "/");
        }
    }
    
    /**
     *  Get the mode-files.
     *  Make use if LEPTON internal core function 'directory_list'.
     *
     */
    public function getModes(): void
    {
        $base_path = dirname(__DIR__)."/CodeMirror/mode";
        $strip = $base_path."/";
        
        LEPTON_handle::register("directory_list");
        directory_list( $base_path, false, 0, self::$instance->modes, $strip );
    }
    
    /**
     *  Returns an assoc. array with the codemirror-files.
     *  To be used e.g. inside 'headers.inc.php' of a given module e.g. 'code2'.
     *
     *  @return array   Assoc. array with 'css' and 'js' as 'main' keys
     */
    public function getBaseFiles(): array
    {
        $basepath = str_replace(LEPTON_PATH . DIRECTORY_SEPARATOR, "", dirname(__DIR__)) . "/CodeMirror";
        
        $aBaseFiles = array(
            'css' => array(
                $basepath."/lib/codemirror.css",
                "/modules/lib_codemirror/css/backend.css"
            ),
            'js' => array(
                $basepath."/lib/codemirror.js",
                $basepath."/mode/xml/xml.js",
                $basepath."/mode/http/http.js",
                $basepath."/addon/mode/multiplex.js",
                $basepath."/mode/htmlembedded/htmlembedded.js",
                $basepath."/mode/htmlmixed/htmlmixed.js",
                $basepath."/mode/css/css.js",
                $basepath."/mode/javascript/javascript.js",
                $basepath."/mode/clike/clike.js",
                $basepath."/mode/php/php.js"
            )
        );

        $aCssPath = $basepath."/theme/";
        
        $ref = (true === $this->useFavorites)
            ? $this->favoritesThemes
            : $this->themes
            ;

        foreach ($ref as $sFilename)
        {
            $aBaseFiles['css'][] = $aCssPath . $sFilename;
        }

        return $aBaseFiles;
    }

    /**
     *  Generate a <select> tag with the 'themes'.
     *
     *  @param  string  $sRef           A reference-name for a given CodeMirror instance.
     *  @param  string  $sAdditionalID  To keep the id unique an additional id-addition. 
     *  @return string  The generated HTML-Code for a simple select-element.
     *
     *  @code{.php}
     *
     *      $oCodeMirror = lib_codemirror::getInstance();
     *      $sMySelect = $oCodeMirror->buildThemeSelect( "code2".$section_id );
     *      // or 
     *      $sMySelect = $oCodeMirror->buildThemeSelect( "code2".$section_id, $section_id );
     *
     *  @endcode
     */    
    public function buildThemeSelect(string $sRef= "", string $sAdditionalID= ""): string
    {
        $sHTML = "<select id='codemirror_themes" . $sAdditionalID . "' onchange=\"" . $sRef . ".setOption('theme', this.options[ this.selectedIndex ].value);" . $sRef . ".refresh();\" >\n";

        $aSource = (true === $this->useFavorites)
            ? $this->favoritesThemes
            : $this->themes
            ;

        foreach ($aSource as $sTheme)
        {
            $sTheme = substr($sTheme, 0, -4);
            $sHTML .= "\n<option value='".$sTheme."' ".($sTheme == $this->defaultTheme ? "selected='selected'" : "").">".$sTheme."</option>";
        }
        $sHTML .= "\n</select>\n";

        return $sHTML;
    }
    
    /**
     *  Generate a <select> tag with the 'modes'.
     *
     *  @param  string  $sRef   A reference-name for a given CodeMirror instance.
     *  @param  string  $sAdditionalID  To keep the id unique an additional id-addition. 
     *  @return string  The generated HTML-Code for a simple select-element.
     *
     *  @code{.php}
     *
     *      $oCodeMirror = lib_codemirror::getInstance();
     *      $sMyModesToUse = $oCodeMirror->buildModeSelect( "code2".$section_id );
     *
     *  @endcode
     */
    public function buildModeSelect(string $sRef = "", string $sAdditionalID = ""): string
    {
        $sHTML = "<select id='codemirror_modes".$sAdditionalID."' onchange=\"".$sRef. ".setOption('theme', this.options[ this.selectedIndex ].value);".$sRef. ".refresh();\" >\n";
        
        $ref = (true === $this->useFavorites)
            ? $this->favoritesModes
            : $this->modes
            ;

        foreach ($ref as $t)
        {
            $t = substr($t,0, -4);
            $sHTML .= "\n<option value='".$t."' ".($t == $this->defaultMode ? "selected='selected'" : "").">".$t."</option>";
        }
        $sHTML .= "\n</select>\n";

        return $sHTML;
    }
    
    /**
     *  Save individual settings for a module.
     *
     *  @param  string  $sModulename    A valid module name.
     *  @param  string  $sTheme         A valid theme name.
     *  @param  string  $sMode          A valid mode name.
     *
     */
    public function saveModuleSpecificSettings(string $sModulename="", string $sTheme="", string $sMode=""): void
    {
        $database = LEPTON_database::getInstance();

        $iTestID = $database->get_one("SELECT `id` FROM `" . TABLE_PREFIX . $this->tableName . "` WHERE `module`='" . $sModulename . "'");
            
        $fields = array(
            'mode' => ($sMode == "") ? $this->defaultMode : $sMode,
            'theme' => ($sTheme == "") ? $this->defaultTheme : $sTheme,
            'module' => $sModulename
        );
        
        $database->build_and_execute(
            (($iTestID === NULL) ? "insert" : "update"),
            TABLE_PREFIX . $this->tableName,
            $fields,
            "`module`='" . $sModulename . "'"
        );
    }

    /**
     *  Get individual settings for a module.
     *  If no settings for the given modulname is found the internal defaults are returned.
     *
     *  @param  string  $sModulename    A valid module name.
     *  @return array   Assoc. array with 'mode' and 'theme'.
     *
     *  @code{.php}
     *
     *      $oCodeMirror = codemirror::getInstance();
     *
     *      $aCode2Set= $oCodeMirror->getModuleSpecificSettings( "code2" );
     *      // or 
     *      $aBakerySet= $oCodeMirror->getModuleSpecificSettings( "bakery" ); 
     *
     *  @endcode
     *
     */    
    public function getModuleSpecificSettings(string $sModulename=""): array
    {
        $database = LEPTON_database::getInstance();
        
        $aSettings = [];
        $database->execute_query(
            "SELECT `theme`,`mode` FROM `" . TABLE_PREFIX . $this->tableName . "` WHERE `module`='" . $sModulename . "'",
            true,
            $aSettings,
            false
        );

        if (empty($aSettings))
        {
            $aSettings['mode']  = $this->defaultMode;
            $aSettings['theme'] = $this->defaultTheme;
        }
        
        return $aSettings;
    }
}
