<?php

/**
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$module_directory   = 'lib_codemirror';
$module_name        = 'CodeMirror library';
$module_function    = 'library';
$module_version     = '5.65.13.0';
$module_platform    = '7.x';
$module_delete      =  true;
$module_author      = 'Dietrich Roland Pehlke (Aldus)';
$module_license     = 'CC 3.0 by-sa';
$module_license_terms   = 'https://creativecommons.org/licenses/by-sa/3.0/de/';
$module_description = 'This module installs basic files of <a href="https://codemirror.net/5/">CodeMirror</a>.';
$module_guid        = '7DF6B380-4370-4D89-A475-D040AB6FAF8F';
$module_home        = 'https://lepton-cms.com';
