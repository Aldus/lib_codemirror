<?php

/**
 * @module          lib_codemirror
 * @author          Dietrich Roland Pehlke (Aldus)
 * @copyright       2017-2023 Dietrich Roland Pehlke (Aldus)
 * @link            https://codemirror.net/5/
 * @license         please see info.php of this module
 * @license_terms   please see info.php of this module
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

header('Content-Type: application/javascript');

$database = LEPTON_database::getInstance();

$oCODEMIRROR = lib_codemirror::getInstance();

//  [1] Get posted values
$aRequest = array(
    'module'    => ['type' => 'string', 'default' => NULL ],
    'mode'      => ['type' => 'string', 'default' => NULL ],
    'theme'     => ['type' => 'string', 'default' => NULL ],
    'section_id'    => ['type' => 'int', 'default' => NULL ],
    'active'    =>  ['type' => 'int', 'default' => 0 ]
);

$aPostedValues = LEPTON_request::getInstance()->testPostValues( $aRequest );

//  [2] Test the posted values for NULL
foreach ($aPostedValues as $name => $value)
{
    if (NULL === $value)
    {
        die(json_encode("err: missing or miss-formed values."));
    }
}

$bHashOk = lib_codemirror_interface::testHash($aPostedValues['section_id']);
if (false === $bHashOk)
{
    //echo json_encode( LEPTON_tools::display( $_SESSION['codemirror_interface_x'] ));
    die(json_encode("err: hash mismatch! (" . lib_codemirror_interface::$error_num . " - " . lib_codemirror_interface::$error_msg . ") "));
}

//  [3] Module installed/known by the system?
$test = $database->get_one("SELECT `addon_id` FROM `" . TABLE_PREFIX . "addons` WHERE `type`='module' AND `directory`='" . $aPostedValues['module'] . "'");
if ($database->is_error())
{
    die(json_encode($database->get_error()));
}
if (NULL === $test)
{
    die(json_encode("err: failed!"));
}

//  [3.1]   Is the section id matching with the module?
$test2 = $database->get_one("SELECT `page_id` FROM `" . TABLE_PREFIX . "sections` WHERE `section_id`=" . $aPostedValues['section_id'] . " AND `module`='" . $aPostedValues['module'] . "'");
if ($database->is_error())
{
    die(json_encode($database->get_error()));
}
if (NULL === $test2)
{
    die(json_encode("err: mismatched!"));
}

//  [4] Is there an entry in the db for this module?
$iTestID = $database->get_one("SELECT `id` FROM `" . TABLE_PREFIX . "mod_lib_codemirror` WHERE `module`='" . $aPostedValues['module'] . "' AND `section_id`=" . $aPostedValues['section_id']);
if ($database->is_error())
{
    die(json_encode($database->get_error()));
}
if (NULL === $iTestID)
{
    $job = "insert";
    $condition = "";
} else {
    $job = "update";
    $condition = "id=".$iTestID;
}

//  [5] Update the db
$database->build_and_execute(
    $job,
    TABLE_PREFIX."mod_lib_codemirror",
    $aPostedValues,
    $condition
);

//  [6] Return some text
if ($database->is_error())
{
    die(json_encode($database->get_error()));
} else {
    // return a messsage
    // echo "'".json_encode($_POST)."'";
    echo json_encode(sprintf(
            $oCODEMIRROR->language['ajax_saved_ok'],
            $aPostedValues['section_id'],
            $aPostedValues['module']
        )
    );
}
