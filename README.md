## lib CodeMirror

[CodeMirror][3] lib for [LEPTON CMS][1] as module.  

Current used version of CodeMirror: see info.php of lib
  
#### License
For this module: [cc 3.0 by-sa][2]  
For CodeMirror: MIT - see the LICENSE

#### Requirements
* PHP 7.3 recommended
* PHP 8.1.2 tested
* [LEPTON CMS][1] (5.3 recommended!)

#### Customize
- Rename the file "lib_codemirror_custom_settings_EXAMPLE" to "lib_codemirror_custom_settings" or  
- Place a file named "lib_codemirror_custom_settings" inside "~/classes" of this module.
content e.g.:  
```code php
class lib_codemirror_custom_settings  
{  
    // [1] A reduce list of themes  
    const CUSTOM_FAVORITES = [  
        'abcdef.css',  
        'ayu-dark.css',  
        'paraiso-light.css',
        'seti.css',
        'vibrant-ink.css',
        'xq-dark.css',
        'yeti.css'
    ];
    
    // [2] Own default theme
    const CUSTOM_DEFAULT = "ayu-dark";
    
}

```

##### Notice
As for the CodeMirror files:
```code

:$ cd <your working directory>
:$ npm install -g bower
:$ bower install --save codemirror

```

[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://creativecommons.org/licenses/by-sa/3.0/de/ "cc 3.0 by-sa"
[3]: https://codemirror.net/ "CodeMirror net"